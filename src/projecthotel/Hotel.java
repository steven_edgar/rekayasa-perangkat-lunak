/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecthotel;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultTreeCellEditor.DefaultTextField;
import org.apache.commons.io.FileUtils;
import static projecthotel.Beranda.baris;
import static projecthotel.Beranda.idHotel;
/**
 *
 * @author Steven Edgar
 */
public class Hotel extends javax.swing.JFrame {
    Beranda beranda;
    /**
     * Creates new form Hotel
     */
    public Hotel() {
        initComponents();
        setEnabled(true);
        hotelDetail();  
        setLocationRelativeTo(this);
    }
    
    
    
    private void hotelDetail(){
        List<String> pathGambar = new ArrayList<String>();
        ArrayList<IsiRuangan> ruanganHotel = daftarRuangan();
        ArrayList<IsiRuangan> teleponHotel = noTelepon();
        DefaultTableModel model = (DefaultTableModel)tableRuangan.getModel();
        
        panelGambar.setLayout(new FlowLayout(FlowLayout.LEFT));
        txtTelepon.setLayout(new FlowLayout(FlowLayout.LEFT));
         
         try{
            Connection con = ConDB.connectDB();
            if(ConDB.user_identity.equals("guest")){
            String sql = "SELECT * FROM hotel,telepon WHERE hotel.id_hotel='"+beranda.idHotel+"' AND telepon.id_hotel='"+beranda.idHotel+"'";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
         
            namaHotel.setText(result.getString("nama_hotel")); 
            txtAlamat.setText(result.getString("alamat_hotel"));
            txtPrice.setText(result.getString("starting_price"));
            txtEmail.setText(result.getString("email"));
            txtFasilitas.setText(result.getString("fasilitas_hotel"));
            
            
            Object[] obj = new Object[3];
            for(int i=0;i<ruanganHotel.size();i++){
                obj[0]  = ruanganHotel.get(i).getKapasitas();
                obj[1] = ruanganHotel.get(i).getKelas();
                obj[2] = ruanganHotel.get(i).getHarga();
                model.addRow(obj);
            }
            
            for(int i=0;i<teleponHotel.size();i++){
                String telp = teleponHotel.get(i).getTelepon();
                txtTelepon.add(new JLabel(telp+", "));
            }
          
            ConDB.nama_Hotel = result.getString("nama_hotel");
            ConDB.id_hotel = result.getInt("id_hotel");
            System.out.print(ConDB.id_hotel);
            
            
            String sql1 = "SELECT * FROM gambar WHERE id_hotel='"+beranda.idHotel+"'AND status_moderasi='true'";
            Statement stmt1 = con.createStatement();
            ResultSet result1 = stmt.executeQuery(sql1);
            Toolkit toolkit=Toolkit.getDefaultToolkit();
            String path=new File(".").getCanonicalPath();
            
            
            while(result1.next()){
                if(result1.getString("status_moderasi").toString().equals("true")){
                    pathGambar.add(result1.getString("path_gambar"));
                }
                
            }
            
            for(String pg:pathGambar){
                Image image = toolkit.getImage(path+"/image/"+pg); 
                Image imagedResized=image.getScaledInstance(150, 150, Image.SCALE_DEFAULT); 
                ImageIcon icon = new ImageIcon(imagedResized);
                panelGambar.add(new JLabel(icon));
            }
           }
            
          con.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
         
        try{
            Connection con = ConDB.connectDB();
            if(ConDB.user_identity.equals("guest")){
                String sql = "SELECT AVG(nilai_rating) FROM rating WHERE id_hotel = '"+beranda.idHotel+"'";
                Statement stmt = con.createStatement();
                ResultSet result = stmt.executeQuery(sql);
                while(result.next()){
                    txtRating.setText(result.getString("AVG(nilai_rating)"));
                }
            }
            con.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
  public ArrayList<IsiRuangan> daftarRuangan(){
      ArrayList<IsiRuangan> listRuangan = new ArrayList<>();
      
      try{
      Connection con = ConDB.connectDB();
      String sql = "SELECT ruangan.kapasitas, ruangan.kelas_ruangan, ruangan.harga_malam FROM ruangan,hotel WHERE ruangan.id_hotel=hotel.id_hotel AND ruangan.id_hotel='"+beranda.idHotel+"'";
      Statement stmt = con.createStatement();
      ResultSet result = stmt.executeQuery(sql);
      
      IsiRuangan isi;
      while(result.next()){
          isi = new IsiRuangan(result.getString("kapasitas"), result.getString("kelas_ruangan"), result.getString("harga_malam"));
          listRuangan.add(isi);
          
      }
      
      }catch(SQLException e){
          JOptionPane.showMessageDialog(null, e);
      }
      return listRuangan;
  }
       
  public ArrayList<IsiRuangan> noTelepon(){
      ArrayList<IsiRuangan> listTelepon = new ArrayList<>();
      
      try{
          Connection con = ConDB.connectDB();
          String sql = "SELECT telepon.no_telp, telepon.status_moderasi FROM telepon, hotel WHERE telepon.id_hotel=hotel.id_hotel AND telepon.id_hotel='"+beranda.idHotel+"'";
          Statement stmt = con.createStatement();
          ResultSet result = stmt.executeQuery(sql);
          
          IsiRuangan isi;
          while(result.next()){
              isi = new IsiRuangan(result.getString("no_telp"));
              if(result.getString("status_moderasi").equals("true")){
              listTelepon.add(isi);
              }
          }
          
      }catch(SQLException e){
          JOptionPane.showMessageDialog(null, e);
      }
      
      return listTelepon;
  }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        jFrame2 = new javax.swing.JFrame();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        namaHotel = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtAlamat = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtPrice = new javax.swing.JTextArea();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtFasilitas = new javax.swing.JTextArea();
        jScrollPane7 = new javax.swing.JScrollPane();
        txtEmail = new javax.swing.JTextArea();
        btnReview = new javax.swing.JButton();
        panelGambar = new javax.swing.JPanel();
        btnRating = new javax.swing.JButton();
        jToggleButton1 = new javax.swing.JToggleButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        tableRuangan = new javax.swing.JTable();
        txtTelepon = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtRating = new javax.swing.JTextField();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jFrame2Layout = new javax.swing.GroupLayout(jFrame2.getContentPane());
        jFrame2.getContentPane().setLayout(jFrame2Layout);
        jFrame2Layout.setHorizontalGroup(
            jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame2Layout.setVerticalGroup(
            jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setEnabled(false);

        jButton1.setText("Home");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHome(evt);
            }
        });

        namaHotel.setEditable(false);
        namaHotel.setColumns(1);
        namaHotel.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        namaHotel.setRows(1);
        namaHotel.setText("Nama Hotel");
        namaHotel.setAutoscrolls(false);
        jScrollPane2.setViewportView(namaHotel);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel1.setText("Alamat");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setText("Starting Price");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel3.setText("Daftar Fasilitas");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel4.setText("Daftar Ruangan");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel5.setText("No. Telepon");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel6.setText("Email");

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        txtAlamat.setEditable(false);
        txtAlamat.setColumns(20);
        txtAlamat.setRows(5);
        jScrollPane1.setViewportView(txtAlamat);

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane3.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        txtPrice.setEditable(false);
        txtPrice.setColumns(20);
        txtPrice.setRows(5);
        jScrollPane3.setViewportView(txtPrice);

        txtFasilitas.setEditable(false);
        txtFasilitas.setColumns(20);
        txtFasilitas.setRows(5);
        jScrollPane5.setViewportView(txtFasilitas);

        jScrollPane7.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane7.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        txtEmail.setEditable(false);
        txtEmail.setColumns(20);
        txtEmail.setRows(5);
        jScrollPane7.setViewportView(txtEmail);

        btnReview.setText("REVIEW");
        btnReview.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReviewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelGambarLayout = new javax.swing.GroupLayout(panelGambar);
        panelGambar.setLayout(panelGambarLayout);
        panelGambarLayout.setHorizontalGroup(
            panelGambarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 460, Short.MAX_VALUE)
        );
        panelGambarLayout.setVerticalGroup(
            panelGambarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 449, Short.MAX_VALUE)
        );

        btnRating.setText("TAMBAH RATING");
        btnRating.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRatingActionPerformed(evt);
            }
        });

        jToggleButton1.setText("TAMBAH GAMBAR ATAU TELEPON");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });

        tableRuangan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Kapasitas", "Kelas", "Harga Per-Malam"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane8.setViewportView(tableRuangan);

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel7.setText("Rating");

        txtRating.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRatingActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jButton1)
                        .addGap(176, 176, 176)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel2))
                                .addGap(10, 10, 10))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel7))
                                .addGap(33, 33, 33)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE)
                            .addComponent(jScrollPane3)
                            .addComponent(jScrollPane5)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtTelepon)
                            .addComponent(txtRating))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(panelGambar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 204, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnReview, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnRating, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jToggleButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(62, 62, 62))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(74, 74, 74)
                                .addComponent(jLabel4)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(panelGambar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnRating, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnReview, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jToggleButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelepon, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(txtRating, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtTelepon.setEditable(false);
        txtRating.setEditable(false);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnReviewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReviewActionPerformed

        this.dispose();
        new Review().setVisible(true);
    }//GEN-LAST:event_btnReviewActionPerformed

    private void btnHome(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHome
        String getRating = txtRating.getText();
        try{
           Connection con = ConDB.connectDB();
           String sql = "UPDATE hotel SET rating='"+getRating+"' WHERE id_hotel='"+beranda.idHotel+"'";
           Statement stmt = con.createStatement();
           stmt.executeUpdate(sql);
           
           this.dispose();
           new Beranda().setVisible(true);
           
           con.close();
        }catch(SQLException e){
            
        }
      
    }//GEN-LAST:event_btnHome

    private void btnRatingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRatingActionPerformed
        // TODO add your handling code here:
        setVisible(false);
        new Rating().setVisible(true);
    }//GEN-LAST:event_btnRatingActionPerformed

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        this.dispose();
        new tambahFotoDanTeleponUser().setVisible(true);
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void txtRatingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRatingActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRatingActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Hotel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Hotel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Hotel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Hotel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Hotel().setVisible(true);
            }
        });
    }

    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRating;
    private javax.swing.JButton btnReview;
    private javax.swing.JButton jButton1;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JFrame jFrame2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JTextArea namaHotel;
    private javax.swing.JPanel panelGambar;
    private javax.swing.JTable tableRuangan;
    private javax.swing.JTextArea txtAlamat;
    private javax.swing.JTextArea txtEmail;
    private javax.swing.JTextArea txtFasilitas;
    private javax.swing.JTextArea txtPrice;
    private javax.swing.JTextField txtRating;
    private javax.swing.JTextField txtTelepon;
    // End of variables declaration//GEN-END:variables
}
