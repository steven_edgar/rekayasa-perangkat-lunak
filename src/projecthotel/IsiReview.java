/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecthotel;

/**
 *
 * @author Joshua Harefa
 */
class IsiReview {
    private int id;
    private String nama_user, isi_review;
    private String tanggal;  
    
    public IsiReview(int id, String nama_user, String isi_review, String tanggal){
        this.id=id;
        this.nama_user=nama_user;
        this.isi_review=isi_review;
        this.tanggal=tanggal;
    }
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the nama_user
     */
    public String getNama_user() {
        return nama_user;
    }

    /**
     * @return the isi_review
     */
    public String getIsi_review() {
        return isi_review;
    }

    /**
     * @return the tanggal
     */
    public String getTanggal() {
        return tanggal;
    }
    
    
  
}
