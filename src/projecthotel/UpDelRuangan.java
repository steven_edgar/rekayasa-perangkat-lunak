/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecthotel;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Steven Edgar
 */
public class UpDelRuangan extends javax.swing.JFrame {

    /**
     * Creates new form UpDelRuangan
     */
    public UpDelRuangan() {
        initComponents();
        getTabelRuangan();
        getDataKapasitasRuangan();
        getDataTipeRuangan();
        setLocationRelativeTo(this);
    }
    
    private void getTabelRuangan(){
        try{
            Connection con = ConDB.connectDB();
            String sql = "SELECT ruangan.id_ruang, ruangan.id_hotel, hotel.nama_hotel, ruangan.kapasitas, ruangan.harga_malam, ruangan.kelas_ruangan FROM ruangan, hotel WHERE ruangan.id_hotel=hotel.id_hotel";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            DefaultTableModel model = (DefaultTableModel)tableRuangan.getModel();
            model.setRowCount(0);
            while(result.next()){
                Object[] obj = new Object[6];
                obj[0] = result.getInt("id_ruang");
                obj[1] = result.getInt("id_hotel");
                obj[2] = result.getString("nama_hotel");
                obj[3] = result.getString("kapasitas");
                obj[4] = result.getString("harga_malam");
                obj[5] = result.getString("kelas_ruangan");
                model.addRow(obj);
            }
            con.close();
        }catch(SQLException e){
            showMessageDialog(null, e);
        }
    }
    
    private void getDataTipeRuangan(){
        try {
            Connection con = ConDB.connectDB();
            String sql = "SELECT * FROM tipe_ruangan";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
             
            while (result.next()) {                
                comboKelas.addItem(result.getString("tipe_ruangan"));
            }
             
            result.last();
            int jumlahdata = result.getRow();
            result.first();
             
        } catch (SQLException e) {
        }
    }
    
    private void getDataKapasitasRuangan(){
        try {
            Connection con = ConDB.connectDB();
            String sql = "SELECT * FROM kapasitas_ruangan";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
             
            while (result.next()) {                
                comboKapasitas.addItem(result.getString("kapasitas_ruangan"));
            }
             
            result.last();
            int jumlahdata = result.getRow();
            result.first();
             
        } catch (SQLException e) {
        }
    }
    
    private void updateRuangan(){
        int baris = tableRuangan.getSelectedRow();
        String idRuangan = tableRuangan.getValueAt(baris, 0).toString();
        String kelas = (String)comboKelas.getSelectedItem();
        String kapasitas = (String)comboKapasitas.getSelectedItem();
        String hargaMalam = txtHarga.getText();
        
        try{
            Connection con = ConDB.connectDB();
            String sql= "UPDATE ruangan SET kapasitas='"+kapasitas+"', harga_malam='"+hargaMalam+"', kelas_ruangan='"+kelas+"' WHERE id_ruang='"+idRuangan+"'";
            Statement stmt = con.createStatement();
            DefaultTableModel model = (DefaultTableModel)tableRuangan.getModel();
            model.setRowCount(0);
            if(!hargaMalam.matches("[0-9]*")){
                showMessageDialog(null,"Harga Harus Berbentuk Angka!!");
            }else{
            stmt.executeUpdate(sql);
            showMessageDialog(null, "Data telah Di Upadate!!");
            
            }
            getTabelRuangan();
            con.close();
        }catch(SQLException e){
            showMessageDialog(null, e);
        }
    }
    
    private void deleteRuangan(){
        int baris = tableRuangan.getSelectedRow();
        String idRuangan = tableRuangan.getValueAt(baris, 0).toString();
        try{
            Connection con = ConDB.connectDB();
            String sql = "DELETE FROM ruangan WHERE id_ruang='"+idRuangan+"'";
            Statement stmt = con.createStatement();
            stmt.executeUpdate(sql);
            showMessageDialog(null, "Data telah Terhapus");
            DefaultTableModel model = (DefaultTableModel)tableRuangan.getModel();
            model.setRowCount(0);
            getTabelRuangan();
            con.close();
        }catch(SQLException e){
            showMessageDialog(null, e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableRuangan = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();
        txtHarga = new javax.swing.JTextField();
        btnDelete = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        comboKapasitas = new javax.swing.JComboBox<>();
        comboKelas = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tableRuangan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id_ruangan", "id_hotel", "nama_hotel", "kapasitas", "harga_malam", "kelas_ruangan"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableRuangan.getTableHeader().setReorderingAllowed(false);
        tableRuangan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableRuanganMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableRuangan);

        btnBack.setText("BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnDelete.setText("DELETE");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnUpdate.setText("UPDATE");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        comboKelas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboKelasActionPerformed(evt);
            }
        });

        jLabel1.setText("Kapasitas");

        jLabel3.setText("Kelas Ruangan");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(comboKelas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3))
                    .addComponent(txtHarga, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 840, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(38, 38, 38))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(btnDelete)
                            .addGap(18, 18, 18)
                            .addComponent(btnUpdate)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(comboKapasitas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(btnBack)
                .addGap(20, 20, 20)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtHarga, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboKapasitas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboKelas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 69, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDelete)
                    .addComponent(btnUpdate))
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboKelasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboKelasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboKelasActionPerformed

    private void tableRuanganMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableRuanganMouseClicked
        int baris = tableRuangan.getSelectedRow();
        txtHarga.setText(tableRuangan.getValueAt(baris, 4).toString());
        comboKelas.setSelectedItem(tableRuangan.getValueAt(baris, 5));
        comboKapasitas.setSelectedItem(tableRuangan.getValueAt(baris, 3));
    }//GEN-LAST:event_tableRuanganMouseClicked

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        updateRuangan();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        deleteRuangan();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.dispose();
        new BerandaAdmin().setVisible(true);
    }//GEN-LAST:event_btnBackActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UpDelRuangan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UpDelRuangan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UpDelRuangan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UpDelRuangan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UpDelRuangan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> comboKapasitas;
    private javax.swing.JComboBox<String> comboKelas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableRuangan;
    private javax.swing.JTextField txtHarga;
    // End of variables declaration//GEN-END:variables
}
