/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecthotel;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Joshua Harefa
 */
public class UpDelTamDaftarRuangan extends javax.swing.JFrame {

    /**
     * Creates new form UpdateDeleteDaftarRuangan
     */
    public UpDelTamDaftarRuangan() {
        initComponents();
        getDataTipe();
        getDataKapasitas();
        setLocationRelativeTo(this);
    }

    
    private void getDataTipe(){
        try{
            Connection con = ConDB.connectDB();
            String sql = "SELECT * FROM tipe_ruangan";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            DefaultTableModel model = (DefaultTableModel)tableTipe.getModel();
            model.setRowCount(0);
            while(result.next()){
                Object[] obj = new Object[2];
                obj[0] = result.getInt("id_tipe_ruangan");
                obj[1] = result.getString("tipe_ruangan");
                model.addRow(obj);
            }
        }catch(SQLException e){
             showMessageDialog(null, e);
        }
    }
    
    private void getDataKapasitas(){
        try{
            Connection con = ConDB.connectDB();
            String sql = "SELECT * FROM kapasitas_ruangan";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            DefaultTableModel model = (DefaultTableModel)tableKapasitas.getModel();
            model.setRowCount(0);
            while(result.next()){
                Object[] obj = new Object[2];
                obj[0] = result.getInt("id_kapasitas_ruangan");
                obj[1] = result.getString("kapasitas_ruangan");
                model.addRow(obj);
            }
        }catch(SQLException e){
             showMessageDialog(null, e);
        }
    }
    
    private void tambahTipe(){
        String tipe = txtTipe.getText();
        try{
            Connection con = ConDB.connectDB();
            String sql1 = "SELECT `tipe_ruangan` FROM tipe_ruangan WHERE `tipe_ruangan`='"+tipe+"'";
            Statement stmt1 = con.createStatement();
            ResultSet result1 = stmt1.executeQuery(sql1);
            
            String sql = "INSERT INTO tipe_ruangan(tipe_ruangan) VALUES('"+tipe+"');";
            Statement stmt = con.createStatement();
            
            if(result1.next()){
                   showMessageDialog(null, "Data Sudah Ada");   
            }else{
                stmt.executeUpdate(sql);
                showMessageDialog(null, "Data Telah Ditambahkan");
                DefaultTableModel model = (DefaultTableModel)tableTipe.getModel();
                model.setRowCount(0);
                getDataTipe();
                getDataKapasitas();
                txtTipe.setText("");
            }
            con.close();
        }catch(SQLException e){
             showMessageDialog(null, e);
        }
    }
    
    private void tambahKapasitas(){
        String kapasitas = txtKapasitas.getText();
        try{
            Connection con = ConDB.connectDB();
            String sql1 = "SELECT `kapasitas_ruangan` FROM kapasitas_ruangan WHERE `kapasitas_ruangan`='"+kapasitas+"'";
            Statement stmt1 = con.createStatement();
            ResultSet result1 = stmt1.executeQuery(sql1);
            
            String sql = "INSERT INTO kapasitas_ruangan(kapasitas_ruangan) VALUES('"+kapasitas+"');";
            Statement stmt = con.createStatement();
            
            if(result1.next()){
                   showMessageDialog(null, "Data Sudah Ada");   
            }else{
                stmt.executeUpdate(sql);
                showMessageDialog(null, "Data Telah Ditambahkan");
                DefaultTableModel model = (DefaultTableModel)tableKapasitas.getModel();
                model.setRowCount(0);
                getDataTipe();
                getDataKapasitas();
                txtKapasitas.setText("");
            }
            con.close();
        }catch(SQLException e){
             showMessageDialog(null, e);
        }
    }
    
    private void updateTipe(){
         int baris = tableTipe.getSelectedRow();
         String idTipe = tableTipe.getValueAt(baris, 0).toString();
         String tipe = txtTipe.getText();
        try{
            Connection con = ConDB.connectDB();
            String sql = "UPDATE `tipe_ruangan` SET tipe_ruangan='"+tipe+"' WHERE id_tipe_ruangan='"+idTipe+"'";
            Statement stmt = con.createStatement();
            stmt.executeUpdate(sql);
            showMessageDialog(null, "Data Telah Di Update");
            DefaultTableModel model = (DefaultTableModel)tableTipe.getModel();
            model.setRowCount(0);
            getDataTipe();
            getDataKapasitas();
            txtTipe.setText("");
            con.close();
        }catch(SQLException e){
             showMessageDialog(null, e);
        }
    }
    
     private void updateKapasitas(){
         int baris = tableKapasitas.getSelectedRow();
         String idKapasitas = tableKapasitas.getValueAt(baris, 0).toString();
         String kapasitas = txtKapasitas.getText();
        try{
            Connection con = ConDB.connectDB();
            String sql = "UPDATE `kapasitas_ruangan` SET kapasitas_ruangan='"+kapasitas+"' WHERE id_kapasitas_ruangan='"+idKapasitas+"'";
            Statement stmt = con.createStatement();
            stmt.executeUpdate(sql);
            showMessageDialog(null, "Data Telah Di Update");
            DefaultTableModel model = (DefaultTableModel)tableKapasitas.getModel();
            model.setRowCount(0);
            getDataTipe();
            getDataKapasitas();
            txtKapasitas.setText("");
            con.close();
        }catch(SQLException e){
             showMessageDialog(null, e);
        }
    }
     
     private void deleteTipe(){
         int baris = tableTipe.getSelectedRow();
         String idTipe = tableTipe.getValueAt(baris, 0).toString();
         
        try{
            Connection con = ConDB.connectDB();
            String sql = "DELETE FROM tipe_ruangan WHERE id_tipe_ruangan='"+idTipe+"'";
            Statement stmt = con.createStatement();
            stmt.executeUpdate(sql);
            showMessageDialog(null, "Data Telah Di Delete");
            DefaultTableModel model = (DefaultTableModel)tableTipe.getModel();
            model.setRowCount(0);
            getDataTipe();
            getDataKapasitas();
            txtTipe.setText("");
            con.close();
        }catch(SQLException e){
             showMessageDialog(null, e);
        }
     }
     
     private void deleteKapasitas(){
         int baris = tableKapasitas.getSelectedRow();
         String idKapasitas = tableKapasitas.getValueAt(baris, 0).toString();
         
        try{
            Connection con = ConDB.connectDB();
            String sql = "DELETE FROM kapasitas_ruangan WHERE id_kapasitas_ruangan='"+idKapasitas+"'";
            Statement stmt = con.createStatement();
            stmt.executeUpdate(sql);
            showMessageDialog(null, "Data Telah Di Delete");
            DefaultTableModel model = (DefaultTableModel)tableKapasitas.getModel();
            model.setRowCount(0);
            getDataTipe();
            getDataKapasitas();
            txtKapasitas.setText("");
            con.close();
        }catch(SQLException e){
             showMessageDialog(null, e);
        }
     }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableTipe = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableKapasitas = new javax.swing.JTable();
        txtKapasitas = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        txtTipe = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tableTipe.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "tipe_ruangan"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableTipe.getTableHeader().setReorderingAllowed(false);
        tableTipe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableTipeMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableTipe);

        tableKapasitas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "kapasitas_ruangan"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableKapasitas.getTableHeader().setReorderingAllowed(false);
        tableKapasitas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableKapasitasMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tableKapasitas);

        jButton1.setText("DELETE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("UPDATE");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("DELETE");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("UPDATE");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel1.setText("TIPE RUANGAN");

        jLabel2.setText("KAPASITAS RUANGAN");

        jButton5.setText("BACK");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("TAMBAH");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText("TAMBAH");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton5)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addGap(33, 33, 33)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                        .addComponent(jButton6))
                    .addComponent(jLabel1)
                    .addComponent(txtTipe))
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(txtKapasitas)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jButton4)
                            .addGap(34, 34, 34)
                            .addComponent(jButton3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                            .addComponent(jButton7))))
                .addGap(85, 85, 85))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtKapasitas, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                    .addComponent(txtTipe))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton4)
                    .addComponent(jButton1)
                    .addComponent(jButton6)
                    .addComponent(jButton7))
                .addContainerGap(84, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableTipeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTipeMouseClicked
        int baris = tableTipe.getSelectedRow();
        txtTipe.setText(tableTipe.getValueAt(baris, 1).toString());
    }//GEN-LAST:event_tableTipeMouseClicked

    private void tableKapasitasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableKapasitasMouseClicked
        int baris = tableKapasitas.getSelectedRow();
        txtKapasitas.setText(tableKapasitas.getValueAt(baris, 1).toString());
    }//GEN-LAST:event_tableKapasitasMouseClicked

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        tambahTipe();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        tambahKapasitas();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        updateTipe();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
       updateKapasitas();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        deleteTipe();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        deleteKapasitas();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        this.dispose();
        new BerandaAdmin().setVisible(true);
    }//GEN-LAST:event_jButton5ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UpDelTamDaftarRuangan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UpDelTamDaftarRuangan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UpDelTamDaftarRuangan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UpDelTamDaftarRuangan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UpDelTamDaftarRuangan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tableKapasitas;
    private javax.swing.JTable tableTipe;
    private javax.swing.JTextField txtKapasitas;
    private javax.swing.JTextField txtTipe;
    // End of variables declaration//GEN-END:variables
}
