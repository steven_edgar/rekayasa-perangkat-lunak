/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecthotel;


import java.awt.Container;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import static projecthotel.Beranda.baris;
import static projecthotel.Beranda.idHotel;
/**
 *
 * @author Joshua Harefa
 */
public class UpdateOrDeleteHotel extends javax.swing.JFrame {
       
    /**
     * Creates new form UpdateOrDeleteHotel
     */
    private DefaultTableModel model;
    ArrayList<JButton> buttonDetail = new ArrayList<JButton>();
    Container buttonLayout;
    
    public static int baris;
    public static String idHotel;
    
    public UpdateOrDeleteHotel() {
         
        initComponents();
        getDataHotel();
        setLocationRelativeTo(this); 
        
        
    }
    
    private void updateHotel(){
      baris = tableHotel.getSelectedRow(); 
      idHotel = tableHotel.getValueAt(baris, 0).toString();
      int ok = JOptionPane.showConfirmDialog(null,"Apakah Yakin Mengubah data ini???", "Confirmation",JOptionPane.YES_NO_CANCEL_OPTION);
      String namaHotel = txtNamaHotel.getText();
      String alamat = txtAlamat.getText();
      String startingPrice = txtPrice.getText();
      String email = txtEmail.getText();
      String fasilitas = txtFasilitas.getText();
      int bintang =Integer.parseInt(sprBintang.getValue().toString());
      String idHotel = tableHotel.getValueAt(baris, 0).toString();
      if(namaHotel.length()<1 || alamat.length()<1 || email.length()<1 || startingPrice.length()<1 || fasilitas.length()<1){
           showMessageDialog(null,"Masih ada Bagian Yang Kosong, Silahkan Coba Lagi !!!");
      }else if(!startingPrice.matches("[0-9]*")){
          showMessageDialog(null,"Starting Price Harus Berupa Angka !!!");
      }else{
    if(ok==0){ 
      try{
          Connection con = ConDB.connectDB();   
          String sql = "UPDATE hotel SET nama_hotel='"+namaHotel+"', alamat_hotel='"+alamat+"', starting_price='"+startingPrice+"', fasilitas_hotel='"+fasilitas+"', bintang='"+bintang+"', email='"+email+"' WHERE id_hotel='"+idHotel+"'";
          Statement stmt = con.createStatement();
          stmt.executeUpdate(sql);
          JOptionPane.showMessageDialog(null, "Update Data Sukses");
          con.close();
          model = (DefaultTableModel)tableHotel.getModel();
          model.setRowCount(0);
          getDataHotel();
        
          
          
          //this.dispose();
      }catch(Exception e){
          JOptionPane.showMessageDialog(null, e);
      }
    }
      }
    }

    private void hapusHotel(){
        int baris = tableHotel.getSelectedRow();
        String idHotel = tableHotel.getValueAt(baris, 0).toString();
        int ok = JOptionPane.showConfirmDialog(null,"Apakah Yakin Mendelete data ini???", "Confirmation",JOptionPane.YES_NO_CANCEL_OPTION);
     if(ok==0) { 
        try
         {
            Connection con = ConDB.connectDB();
            String sql="DELETE FROM hotel WHERE id_hotel='"+idHotel+"'";
            Statement stmt = con.createStatement();
            stmt.executeUpdate(sql);
            
            String sqlTelepon="DELETE FROM telepon WHERE id_hotel='"+idHotel+"'";
            Statement stmtTelepon = con.createStatement();
            stmtTelepon.executeUpdate(sqlTelepon);
              
            String sqlGambar="DELETE FROM gambar WHERE id_hotel='"+idHotel+"'";
            Statement stmtGambar = con.createStatement();
            stmtGambar.executeUpdate(sqlGambar);
            
            String sqlRating="DELETE FROM rating WHERE id_hotel='"+idHotel+"'";
            Statement stmtRating = con.createStatement();
            stmtRating.executeUpdate(sqlRating);
            
            String sqlReview="DELETE FROM review WHERE id_hotel='"+idHotel+"'";
            Statement stmtReview = con.createStatement();
            stmtReview.executeUpdate(sqlReview);
            
            String sqlRuangan="DELETE FROM ruangan WHERE id_hotel='"+idHotel+"'";
            Statement stmtRuangan = con.createStatement();
            stmtRuangan.executeUpdate(sqlRuangan);
            
            JOptionPane.showMessageDialog(null, "Delete Data Sukses");
            con.close();
            model =  (DefaultTableModel)tableHotel.getModel();
            model.setRowCount(0);

            getDataHotel();
            
            //this.dispose();
            
         }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Delete Data Gagal");
        }
     }
       
    }
    
    private void getDataHotel(){

        try{
        Connection con = ConDB.connectDB();
        String sql = "SELECT * FROM hotel";
        Statement stmt = con.createStatement();
        ResultSet result =  stmt.executeQuery(sql);
        
        DefaultTableModel model1 = (DefaultTableModel)tableHotel.getModel();
        model1.setRowCount(0);
        
//        for(int i =1;i <= 3;i++){
//        buttonLayout = getContentPane();
//        buttonLayout.setLayout(new GridLayout(7,6));
//       JButton button = new JButton();
//            buttonDetail.add(button);
//            buttonLayout.add(button);
//            button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent ae) {
//               new Login().setVisible(true);
//            }
//        });
//        }
             
        
        while(result.next()){
            Object[] obj = new Object[7];
            obj[0] = result.getInt("id_hotel");
            obj[1] = result.getString("nama_hotel");
            obj[2] = result.getString("alamat_hotel");
            obj[3] = result.getInt("bintang");
            obj[4] = result.getString("starting_price");
            obj[5] = result.getString("fasilitas_hotel");
            obj[6] = result.getString("email");
            model1.addRow(obj);   
        }
        con.close();
        }catch(SQLException e){
          showMessageDialog(null,e.getMessage(),"Error!",JOptionPane.ERROR_MESSAGE);  
        }
    }
    
 
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnDelete = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableHotel = new javax.swing.JTable();
        txtNamaHotel = new javax.swing.JTextField();
        txtAlamat = new javax.swing.JTextField();
        txtPrice = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        sprBintang = new javax.swing.JSpinner();
        jLabel7 = new javax.swing.JLabel();
        txtFasilitas = new javax.swing.JTextField();
        btnRuangan = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnDelete.setText("DELETE");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnUpdate.setText("UPDATE");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Exotc350 DmBd BT", 0, 24)); // NOI18N
        jLabel1.setText("Sistem Informasi Hotel");

        tableHotel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id_hotel", "Nama Hotel  ", "Alamat Hotel", "Bintang", "Starting Price", "Fasilitas Hotel", "Email"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableHotel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableHotelMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableHotel);

        jLabel2.setText("NAMA HOTEL");

        jLabel3.setText("ALAMAT");

        jLabel4.setText("STARTING PRICE");

        btnBack.setText("BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel5.setText("EMAIL");

        jLabel6.setText("BINTANG");

        jLabel7.setText("FASILITAS");

        btnRuangan.setText("RUANGAN");
        btnRuangan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRuanganActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnRuangan, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(14, 14, 14))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtAlamat)
                                            .addComponent(txtPrice)
                                            .addComponent(txtNamaHotel)
                                            .addComponent(txtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE))
                                        .addGap(96, 96, 96)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel7))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(sprBintang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtFasilitas, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 614, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 126, Short.MAX_VALUE)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(sprBintang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNamaHotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAlamat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFasilitas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRuangan, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        int min=1;
        int max=5;
        int step=1;
        int initValue=1;
        SpinnerModel model=new SpinnerNumberModel(initValue, min, max, step);

        sprBintang.setModel(model);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        hapusHotel();      
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void tableHotelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableHotelMouseClicked
            int baris = tableHotel.getSelectedRow(); 
            txtNamaHotel.setText(tableHotel.getValueAt(baris, 1).toString());
            txtAlamat.setText(tableHotel.getValueAt(baris, 2).toString());
            sprBintang.setValue(tableHotel.getValueAt(baris, 3));
            txtPrice.setText(tableHotel.getValueAt(baris, 4).toString());
            txtFasilitas.setText(tableHotel.getValueAt(baris, 5).toString());
            txtEmail.setText(tableHotel.getValueAt(baris, 6).toString());
    }//GEN-LAST:event_tableHotelMouseClicked

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        updateHotel();
       
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        setVisible(false);
        new BerandaAdmin().setVisible(true);
        this.dispose();
        
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnRuanganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRuanganActionPerformed
        // TODO add your handling code here:
        baris = tableHotel.getSelectedRow();
        idHotel = tableHotel.getValueAt(baris, 0).toString();
        
        try{
            Connection con = ConDB.connectDB();
        
            String sql = "SELECT * FROM hotel WHERE id_hotel='"+idHotel+"'";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            this.dispose();
            new Ruangan().setVisible(true);
            
          con.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
        
        
    }//GEN-LAST:event_btnRuanganActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UpdateOrDeleteHotel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UpdateOrDeleteHotel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UpdateOrDeleteHotel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UpdateOrDeleteHotel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UpdateOrDeleteHotel().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnRuangan;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner sprBintang;
    private javax.swing.JTable tableHotel;
    private javax.swing.JTextField txtAlamat;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtFasilitas;
    private javax.swing.JTextField txtNamaHotel;
    private javax.swing.JTextField txtPrice;
    // End of variables declaration//GEN-END:variables
}
