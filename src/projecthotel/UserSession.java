/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecthotel;

/**
 *
 * @author Joshua Harefa
 */
public class UserSession {
    private static int idUser;
    private static String username;
    private static String nama;
    private static String password;
    private static String userIdentity;
    
    public static int getIdUser(){
        return idUser;
    }
    
    public static void setIdUser(int idUser){
        UserSession.idUser = idUser;
    }
    
    
    public static String getUsername(){
        return username;
    }
    
    public static void setUsername(String username){
        UserSession.username = username;
    }
    
    public static String getNama(){
        return nama;
    }
    
    public static void setNama(String nama){
        UserSession.nama = nama;
    }
    
    
    public static String getPassword(){
        return password;
    }
    
    public static void setPassword(String password){
        UserSession.password = password;
    }
    
    public static String getUserIdentity(){
        return userIdentity;
    }
    
    public static void setUserIdentity(String userIdentity){
        UserSession.userIdentity = userIdentity;
    }
}
