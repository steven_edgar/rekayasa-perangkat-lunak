/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecthotel;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Joshua Harefa
 */
public class tambahFotoDanTeleponUser extends javax.swing.JFrame {
     JFileChooser chooser = new JFileChooser();
    /**
     * Creates new form tambahFotoDanTeleponUser
     */
    public tambahFotoDanTeleponUser() {
        initComponents();
        setLocationRelativeTo(this);
        namaHotel.setText(ConDB.nama_Hotel);
    }
    
    private void tambahNoTelepon(){
        String noTelepon = txtTelepon.getText();
        String statusModerasi = "false";
        
         try{
            Connection con = ConDB.connectDB();
            String Sql = "INSERT INTO telepon(no_telp,id_hotel,id_user,status_moderasi) VALUES('"+noTelepon+"','"+ConDB.id_hotel+"','"+ConDB.id_user+"','"+statusModerasi+"');";
            Statement stmt = con.createStatement();
            stmt.executeUpdate(Sql);
            showMessageDialog(null,"Nomor Telepon Is send!!!");
            con.close();
        }catch(SQLException e){
            showMessageDialog(null,e.getMessage(),"Error!",JOptionPane.ERROR_MESSAGE);  
        }
    }
    
    private void submitGambar(){
         String foto = txtFoto.getText();
         String statusModerasi = "false";
        
         try{
            Connection con = ConDB.connectDB();
            String Sql = "INSERT INTO gambar(id_hotel,id_user,path_gambar,status_moderasi) VALUES('"+ConDB.id_hotel+"','"+ConDB.id_user+"','"+foto+"','"+statusModerasi+"');";
            Statement stmt = con.createStatement();
            stmt.executeUpdate(Sql);
            showMessageDialog(null,"Gambar Telah Dimasukkan!!!");
            con.close();
        }catch(SQLException e){
            showMessageDialog(null,e.getMessage(),"Error!",JOptionPane.ERROR_MESSAGE);  
        }
    }
    
     private void tambahGambar(){
         String foto = txtFoto.getText();
         String statusModerasi = "false";
         
        FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG,GIF,and PNG Images","jpg","JPG","png");
        chooser.setFileFilter(filter);
        chooser.showOpenDialog(null);
        File file = chooser.getSelectedFile();
        try {
            Image image = ImageIO.read(file);
            ImageIcon imageIcon = new ImageIcon(image.getScaledInstance(100, 100, 100));
            txtFoto.setText(chooser.getSelectedFile().getName());   
            gambarHotel.setIcon(imageIcon);
            String path=new File(".").getCanonicalPath();
            FileUtils.copyFileToDirectory(file, new File(path+"/image"));
        } catch (IOException ex) {
            Logger.getLogger(CreateHotel.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtFoto = new javax.swing.JTextField();
        btnFoto = new javax.swing.JToggleButton();
        txtTelepon = new javax.swing.JTextField();
        submitFoto = new javax.swing.JToggleButton();
        submitTelepon = new javax.swing.JToggleButton();
        jLabel1 = new javax.swing.JLabel();
        btnBack = new javax.swing.JToggleButton();
        namaHotel = new javax.swing.JLabel();
        gambarHotel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnFoto.setText("Browse");
        btnFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFotoActionPerformed(evt);
            }
        });

        txtTelepon.setText("Masukkan Nomor Telepon");

        submitFoto.setText("Submit");
        submitFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitFotoActionPerformed(evt);
            }
        });

        submitTelepon.setText("Submit");
        submitTelepon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitTeleponActionPerformed(evt);
            }
        });

        jLabel1.setText("TAMBAH FOTO ATAU NOMOR TELEPON HOTEL");

        btnBack.setText("BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(submitTelepon, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelepon, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(submitFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(namaHotel, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 164, Short.MAX_VALUE)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(gambarHotel, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(namaHotel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(94, 94, 94)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(submitFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(txtTelepon, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(gambarHotel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(submitTelepon, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(118, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void submitTeleponActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitTeleponActionPerformed
       tambahNoTelepon();
    }//GEN-LAST:event_submitTeleponActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.dispose();
        new Hotel().setVisible(true);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFotoActionPerformed
        tambahGambar();
    }//GEN-LAST:event_btnFotoActionPerformed

    private void submitFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitFotoActionPerformed
        submitGambar();
    }//GEN-LAST:event_submitFotoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(tambahFotoDanTeleponUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(tambahFotoDanTeleponUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(tambahFotoDanTeleponUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(tambahFotoDanTeleponUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new tambahFotoDanTeleponUser().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnBack;
    private javax.swing.JToggleButton btnFoto;
    private javax.swing.JLabel gambarHotel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel namaHotel;
    private javax.swing.JToggleButton submitFoto;
    private javax.swing.JToggleButton submitTelepon;
    private javax.swing.JTextField txtFoto;
    private javax.swing.JTextField txtTelepon;
    // End of variables declaration//GEN-END:variables
}
